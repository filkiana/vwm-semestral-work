import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.patches as patch
from mpl_toolkits.mplot3d import Axes3D
import copy
import os


cnt = 1

''' 
    Plots one rectangle
    @param[in] rectangle is a Node.bounding_rectangle 
'''
def plot_rec(rectangle, color='b', ls='-', lw=3.0):

    rec = patch.Rectangle((rectangle[0][0], rectangle[1][0]), abs(rectangle[0][1]-rectangle[0][0]), abs(rectangle[1][0]-rectangle[1][1]), linewidth=1, facecolor='none', lw=lw, edgecolor=color, linestyle=ls)

    plt.gca().add_patch(rec)

    plt.text(rectangle[0][0], rectangle[1][0], str(cnt), color="r")

    global cnt
    cnt += 1


    plt.xlim([0, 7])
    plt.ylim([0, 7])


class Node:
    def __init__(self, number_of_children, parent):
        self.parent = parent
        self.amount_of_children = 0
        self.children = []
        self.leaf = False
        self.mbr = None


class RTree:
    def __init__(self, max_number_of_children):
        self.max_number_of_children = max_number_of_children
        self.min_number_of_children = 2
        self.root = Node(max_number_of_children, None)
        os.mkdir("nodes")

    def insert(self, block, node = None):
        if node == None:
            node = self.root
            if node.amount_of_children == 0: # It means we have no nodes for now
                buff = Node(self.max_number_of_children, node)
                buff.leaf = True
                buff.children.append(block)
                buff.amount_of_children += 1

                buff.mbr = block

                node.amount_of_children += 1
                node.children.append(buff)
                return True

        
        if node.leaf == True:
            # Control overflow
            if node.amount_of_children == self.max_number_of_children:
                node.children.append(block)
                new_nodes = self.split_node(node.children)


                new_nodes[0].parent = node.parent
                new_nodes[1].parent = node.parent

                node.parent.children.remove(node)
                node.parent.children.append(new_nodes[0])
                node.parent.children.append(new_nodes[1])
                node.parent.amount_of_children += 1

                return False # Indicates that node was splited, now parent can have overflow too

            node.children.append(block)
            for child in node.children:
                    node.mbr = self.find_right_mbr(node.mbr, block)

            node.amount_of_children += 1
            return True


        right_subtree = self.choose_leaf(node, block)

        if not self.insert(block, right_subtree): # Child node was splited
            if node.amount_of_children > self.max_number_of_children: # Current node has overflow
                
                new_nodes = self.split_node(node.children)

                if node == self.root:
                    new_root = Node(self.max_number_of_children, None)
                    new_root.children.append(new_nodes[0])
                    new_root.children.append(new_nodes[1])
                    new_root.amount_of_children += 2
                    new_root.mbr = self.find_right_mbr_for_node(new_root)

                    self.root = new_root

                else:
                    new_nodes[0].parent = node.parent
                    new_nodes[1].parent = node.parent

                    node.parent.children.remove(right_subtree)
                    node.parent.children.append(new_nodes[0])
                    node.parent.children.append(new_nodes[1])
                    node.parent.amount_of_children += 1
                
                return False


        node.amount_of_children = len(node.children)
        node.mbr = self.find_right_mbr_for_node(node)
        return True



    ''' This method is called by insert method.
        Finds child node (@class Node) for node (@param[in] node) which is more sutable for block (@param[in] block)      
    '''
    def choose_leaf(self, node, block):
        best_choise = []
        
        for i in range(node.amount_of_children):
            best_choise.append(self.find_area_enlargement(node.children[i].mbr, block))

        return node.children[best_choise.index(min(best_choise))]

    def find_right_mbr_for_node(self, node):
        if node.leaf == True:
            final_mbr = node.mbr
            for block in node.children:
                final_mbr = self.find_right_mbr(final_mbr, block)
        else:
            final_mbr = node.mbr
            for block in node.children:
                final_mbr = self.find_right_mbr(final_mbr, block.mbr)

        return final_mbr


    ''' Method finds blocks from (@values) for which distance between them is the biggest
        Returns two nodes [Node] 
    '''
    def find_bigger_dist(self, values):
        results = []
        axesZ = False

        tmp = np.array(values)

        w_x = max(tmp[:, 0, 1]) - min(tmp[:, 0, 0])
        w_y = max(tmp[:, 1, 1]) - min(tmp[:, 1, 0])

        # print("SHAPE: ", tmp.shape)
        w = max(w_x, w_y)

        if tmp.shape[1] == 3:
            axesZ = True
            w_z = abs(max(tmp[:, 2, 1]) - min(tmp[:, 2, 0]))
            w = max(w, w_z)


        tmp_x_1 = tmp[:, 0, 0]
        tmp_x_2 = tmp[:, 0, 1]
        


        x_axis_l_sum = np.empty([len(values), len(values)])

        for index_i, i in enumerate(tmp_x_1):
            for index_j, j in enumerate(tmp_x_2):
                x_axis_l_sum[index_i, index_j] = i - j
        x_axis_l_sum = x_axis_l_sum / float(w)

        tmp_y_1 = tmp[:, 1, 0]
        tmp_y_2 = tmp[:, 1, 1]

        y_axis_l_sum = np.empty([len(values), len(values)])

        for index_i, i in enumerate(tmp_y_1):
            for index_j, j in enumerate(tmp_y_2):
                y_axis_l_sum[index_i, index_j] = i - j
        y_axis_l_sum = y_axis_l_sum / float(w)
        

        if axesZ:
            tmp_z_1 = tmp[:, 2, 0]
            tmp_z_2 = tmp[:, 2, 1]

            z_axis_l_sum = np.empty([len(values), len(values)])

            for index_i, i in enumerate(tmp_z_1):
                for index_j, j in enumerate(tmp_z_2):
                    z_axis_l_sum[index_i, index_j] = i - j
            z_axis_l_sum = z_axis_l_sum / float(w)

        
        if axesZ:
            overall_l = np.sum((x_axis_l_sum, y_axis_l_sum, z_axis_l_sum), axis=0)
        else:
            overall_l = np.sum((x_axis_l_sum, y_axis_l_sum), axis=0)

        index = np.where(overall_l == overall_l.max())
        index = (index[0][0], index[1][0])
        

        a = Node(self.max_number_of_children, None)
        a.mbr = tmp[index[0]]
        a.children.append(tmp[index[0]])

        b = Node(self.max_number_of_children, None)
        b.mbr = tmp[index[1]]
        b.children.append(tmp[index[1]])

        return tmp[index[0]], tmp[index[1]]


    def is_block_in_node(self, node, value):
        # for child in node.children:
        #     if child.mbr[0][0] == value[0][0] and child.mbr[0][1] == value[0][1] \
        #         and child.mbr[1][0] == value[1][0] and child.mbr[1][1] ==value[1][1]:
        #             return True
        for child in node.children:
            if np.array_equal(child, value):
                return True
        return False


    def find_better_node(self, node1, node2, value):
        if self.is_block_in_node(node1, value) or self.is_block_in_node(node2, value):
            return False
        if self.find_area_enlargement(node1.mbr, value) < self.find_area_enlargement(node2.mbr, value):
            return node1
        else:
            return node2


    ''' Splits overfilled node '''
    def split_node(self, values):

        first = Node(self.max_number_of_children, None)
        second = Node(self.max_number_of_children, None)

        if isinstance(values[0], Node): # It's node
            values_ = []
            for i in values:
                values_.append(np.array(i.mbr))

            block1, block2 = self.find_bigger_dist(values_)


            for index, i in enumerate(values):
                if self.min_number_of_children - first.amount_of_children == len(values) - index:
                    for j in range(index, len(values)):    
                        first.children.append(values[j])
                        first.amount_of_children += 1

                    break

                elif self.min_number_of_children - second.amount_of_children == len(values) - index:
                    for j in range(index, len(values)):
                        second.children.append(values[j])
                        second.amount_of_children += 1
                    break

                right_node = self.find_better_node(first, second, i.mbr)
                if right_node != False:
                    right_node.children.append(i)
                    right_node.mbr = self.find_right_mbr_for_node(right_node)
                    right_node.amount_of_children += 1



        else: # It's leaf
            block1, block2 = self.find_bigger_dist(values)
            first.children.append(block1)
            first.leaf = True
            first.mbr = block1

            second.children.append(block2)
            second.leaf = True
            second.mbr = block2

            for index, block in enumerate(values):
                right_node = self.find_better_node(first, second, block)

                if self.min_number_of_children - len(first.children) == len(values) - index:
                    for j in range(index, len(values)):    
                        first.children.append(values[j])
                        first.amount_of_children += 1
                    break
            
                elif self.min_number_of_children - len(second.children) == len(values) - index:
                    for j in range(index, len(values)):
                        second.children.append(values[j])
                        second.amount_of_children += 1
                    break

                if right_node != False:
                    print ("mbr: {}".format(right_node.mbr))
                    right_node.children.append(block)
                    right_node.amount_of_children += 1
                    right_node.mbr = self.find_right_mbr_for_node(right_node)


        first .mbr = self.find_right_mbr_for_node(first)
        first.amount_of_children = len(first.children)

        second.mbr = self.find_right_mbr_for_node(second)
        second.amount_of_children = len(second.children)

        return first, second



    ''' Finds площадь '''
    def __area_index(self, area):
        area = np.array(area)
        if area.shape[0] == 3:
            return abs((area[0][1] - area[0][0]) * (area[1][1] - area[1][0]) * (area[2][1] - area[2][0]))
        return abs((area[0][1] - area[0][0]) * (area[1][1] - area[1][0]))


    ''' Method returns new size of block (@bb_) '''
    def find_right_mbr(self, bb_, block_):
        try:
            if bb_ == None:
                return block_
        except ValueError:
            pass

        if isinstance(bb_, list):
            bb_ = np.array(bb_)

        
        block = copy.deepcopy(block_)
        bb = copy.deepcopy(bb_)
        if bb[0][1] < block[0][1]:
            bb[0][1] = block[0][1]
        if bb[1][1] < block[1][1]:
            bb[1][1] = block[1][1]
        if bb[0][0] > block[0][0]:
            bb[0][0] = block[0][0]
        if bb[1][0] > block[1][0]:
            bb[1][0] = block[1][0]

        if bb_.shape[0] == 3:
            bb[2, 0] = min(bb_[2][0], block[2][0])
            bb[2, 1] = max(bb_[2][1], block[2][1])

        return bb


    ''' Method finds difference between areas '''
    def find_delta(self, bb, tmp):
        try:
            if tmp == None:
                return self.__area_index(bb)
        except ValueError:
            pass
        return self.__area_index(bb) - self.__area_index(tmp)


    ''' Method finds enlargement of block (@bb) which is needed to absorb block (@tmp) '''
    def find_area_enlargement(self, bb, tmp):
        buff = self.find_right_mbr(bb, tmp)
        return self.find_delta(buff, bb)


    def choose_right_node(self):
        pass

    def delete(self):
        pass

    '''examine if mbr1 intersect mbr2'''
    # def is_intersect(self, mbr1, mbr2):
    #     xmax = max(mbr1[0][0], mbr2[0][0])
    #     ymax = max(mbr1[0][1], mbr2[0][1])
    #     xmin = min(mbr1[1][0], mbr2[1][0])
    #     ymin = min(mbr1[1][1], mbr2[1][1])
    #     if xmax > xmin or ymax > ymin:
    #         return False
    #     else:
    #         return True

    def is_intersect(self, mbr1, mbr2):
        left = max(mbr1[0][0], mbr2[0][0])
        bottom = max(mbr1[1][0], mbr2[1][0])
        right = min(mbr1[0][1], mbr2[0][1])
        top = min(mbr1[1][1], mbr2[1][1])
        if left >= right or bottom >= top:
            return False
        if mbr1[1][0] > mbr2[1][1] or mbr2[1][0] > mbr1[1][1]:
            return False
        
        return True

    '''Finds all rectangles that are stored in an R-tree with root node (@node), which are intersected by a query rectangle (@query). Answers are stored in the set A '''
    def range_search(self, query, node = None):
        if node == None:
            node = self.root
        answer = []
        if node.leaf == False:
            '''examine each entry e of RN to find those e.mbr that intersect Q '''
            '''foreach such entry e call RangeSearch(e.ptr,Q)'''
            for child in node.children:
                if self.is_intersect(child.mbr, query):
                    answer = answer + self.range_search(query, child)

        else:
            '''examine all entries e and find those for which e.mbr intersects Q '''
            '''add these entries to the answer set A'''
            for child in node.children:
                if self.is_intersect(child, query):
                    answer.append(child)
            

        return answer

    def print_tree(self, node, cnt):
        colors = ['r', 'g', 'y']
        color_index = 0

        print ("\n\nPRINT_TREE:\n")
        print (self.root.amount_of_children)
        for child in self.root.children:
            print (child.amount_of_children)
            for i in child.children:
                print ("amount_of_children: {}".format(i.amount_of_children))
                plot_rec(i.mbr, colors[color_index % 3], ':', 5.0)
                color_index += 1

    def control(self, node):

        if not node.leaf:
            for child in node.children:
                self.control(child)
        
        print (node.mbr, node.amount_of_children)


    def toFile(self, node):
        c = {
            "id" : node.id,
            'parent' : str(node.parent),
            'amount_of_children' : node.amount_of_children,
            'children' : str(node.children),
            'leaf' : str(node.leaf),
            'mbr' : str(node.mbr)
            }

        df = pd.DataFrame(data=c, index=[0])
        df.to_csv('nodes/{}.csv'.format(node.id), sep=',', index=False)

    ''' Returns Node object '''
    def fromFile(self, node_id):
        df = pd.read_csv('nodes/{}.csv'.format(node_id), sep=',', encoding='utf-8', usecols=['id', 'parent', 'children', 'amount_of_children', 'leaf', 'mbr'])

        node = Node(node_id, 0, None)
        node.parent = df['parent'].values[0]
        node.amount_of_children = df['amount_of_children'].values[0]
        node.children = df['children'].values[0]
        node.leaf = df['leaf'].values[0]
        node.mbr = df['mbr'].values[0]

        return node


a = RTree(3)

if False:
    block = ([[1, 2], [1, 2]])
    a.insert(block)
    plot_rec(block)

    block = ([[1, 2], [3, 5]])
    a.insert(block)
    plot_rec(block)

    block = ([[3, 5], [5, 6]])
    a.insert(block)
    plot_rec(block)

    block = ([[3, 4], [3, 4]])
    a.insert(block)
    plot_rec(block)

    block = ([[3, 4], [1, 2]])
    a.insert(block)
    plot_rec(block)

    block = ([[5, 6], [1, 4]])
    a.insert(block)
    plot_rec(block)

    block = ([[2, 3], [2, 3]])
    a.insert(block)
    plot_rec(block)

    ###

    block = ([[3, 4], [2, 3]])
    a.insert(block)
    plot_rec(block)

    block = ([[2, 3], [1, 2]])
    a.insert(block)
    plot_rec(block)

    block = ([[1, 2], [2, 3]])
    a.insert(block)
    plot_rec(block)

    block = ([[4, 6], [2, 3]])
    a.insert(block)
    plot_rec(block)




    cnt = 0
    a.print_tree(a.root, cnt)


    print ("SEARCH RESULT: ")
    if False: test = ([[3, 5], [1, 3]])
    if True: test = ([[5, 6], [1, 6]])

    ans = a.range_search(test)
    for rec in ans:
        plot_rec(rec, 'YELLOW')
    plot_rec(test, 'CYAN', ls='-.')


    print (ans)


    plt.show()

fig = plt.figure()
ax = plt.axes(projection='3d')


block = ([[0, 1], [1, 1], [1, 2]])
ax.scatter(block[0], block[1], block[2])
a.insert(block)

block = ([[0, 2], [0, 2], [0, 2]])
ax.scatter(block[0], block[1], block[2])
a.insert(block)

block = ([[0, 1], [0, 1], [0, 1]])
ax.scatter(block[0], block[1], block[2])
a.insert(block)

block = ([1, 2], [1, 2], [1, 2])
ax.scatter(block[0], block[1], block[2])
a.insert(block)

block = ([1, 2], [0, 1], [0, 1])
ax.scatter(block[0], block[1], block[2])
a.insert(block)


print ("\n\n\n")
for i in a.root.children:
    print (i.mbr)


plt.show()


''' 
TEST DATA:

block = ([[1, 2], [1, 2]])
block = ([[1, 2], [3, 5]])
block = ([[3, 5], [5, 6]])
block = ([[3, 4], [3, 4]])
block = ([[3, 4], [1, 2]])
block = ([[5, 6], [1, 4]])
'''
